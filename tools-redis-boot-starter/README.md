# 引入Maven 坐标
```xml
	<dependency>
            <groupId>com.ark.frigate</groupId>
            <artifactId>tools-redis-boot-starter</artifactId>
            <version>1.0-SNAPSHOT</version>
	</dependency>

```

#主要功能

Redis Lock注解，依赖以下注解，并且需要初始化 JedisUtil 类，具体实现由
RedisLockService类负责
JedisUtil接口 项目内部实现

@CacheManage  对请求方法结果进行缓存，结果为null时不进行缓存
<table>
    <tr><th>参数</th><th>说明</th><th>备注</th></tr>
    <tr><td>key</td><td>指定缓存Key</td><td></td></tr>
    <tr><td>args[]</td><td>在Key中追加方法的参数</td><td></td></tr>
    <tr><td>timeout</td><td>缓存时间 默认300秒（5分钟）</td><td>单位秒</td></tr>
</table>


@LockManage 分布式锁
<table>
    <tr><th>参数</th><th>说明</th><th>备注</th></tr>
    <tr><td>key</td><td>指定Lock的Key</td><td></td></tr>
    <tr><td>args[]</td><td>在Key中追加方法参数</td><td></td></tr>
    <tr><td>timeout</td><td>默认10秒</td><td>单位秒</td></tr>
    <tr><td>autoRelease</td><td>方法执行完立即释放，默认释放</td><td>true/false</td></tr>
</table>


Redis Cache注解


#使用方法

引入包后，必须实现JedisUtil类，并将其注入到Spring容器 例：

```java

import com.ark.frigate.redis.JedisUtil;
import org.springframework.stereotype.Component;

@Component
public class MyRedisComponent implements JedisUtil {

    boolean delKey(String key) {
        // TODO
    }
    
    String getValue(String key) {
        // TODO
    }
    
    boolean setValue(String key, String value, int seconds) {
        // TODO
    }

    boolean setValue(String key, String value, String nxxx, int seconds) {
        // TODO
    }
}


```
@CacheManage 对方法请求结果进行10s的缓存，⚠️这里将结果存入Redis时，使用的是Json序列化，对于不支持的对象可能会抱错，具体的序列化实现可以自行进行修改。
CacheManageAspect 类型 例：

```java

import org.springframework.stereotype.Service;

@Service
public class MyService {

    @CacheManage(key = "GET_PLACE_CREATE_ORDER")
    public Long countOrder(String userId, OrderCreateVo orderCreateVo) {

    }
}

```

@LockManage 防止用户重复请求，在Key中加入userId  例：
```java

import org.springframework.stereotype.Service;

@Service
public class MyService {
    @LockManage(key = "PLACE_CREATE_ORDER_", args = 0)
    public Long createOrder(String userId, OrderCreateVo orderCreateVo) {
        // TODO
    }
}

```
