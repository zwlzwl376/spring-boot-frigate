package com.ark.frigate.redis.lock.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zengweilong
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LockManage {

    /**
     * 默认true：执行完是否立即主动释放Lock
     * false 执行完后，让redis自动释放
     *
     * @return
     */
    boolean autoRelease() default true;

    /**
     * key
     *
     * @return
     */
    String key() default "";

    /**
     * 变量
     *
     * @return
     */
    int[] args() default {};

    /**
     * 默认有效时间 单位 秒
     *
     * @return
     */
    int timeout() default 10;

}
