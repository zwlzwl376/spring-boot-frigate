package com.ark.frigate.redis;

/**
 * TODO
 *
 * @author zengweilong
 * @date 6/3/21 10:16 AM
 */
public interface JedisUtil {

    /**
     * @param key
     * @return
     */
    boolean delKey(String key);

    /**
     * @param key
     * @return
     */
    String getValue(String key);

    /**
     * @param key
     * @param value
     * @param seconds
     * @return
     */
    boolean setValue(String key, String value, int seconds);

    /**
     * 缓存value
     *
     * @param nxxx NX|XX, NX -- Only set the key if it does not already exist. XX
     *             -- Only set the key
     */
    boolean setValue(String key, String value, String nxxx, int seconds);
}
