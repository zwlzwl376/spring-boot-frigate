package com.ark.frigate.redis.lock;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author zengweilong
 */
public class ManageAspect {

    public final static String COMMA = ",";

    public final static String SPOT = ".";

    public final static String LINE = "_";

    public String getKey(Object[] argsArray, int[] args) {
        StringBuilder key = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if (args[i] < argsArray.length) {
                key.append(argsArray[args[i]]);
            }
        }
        return key.toString();
    }

    public String getKey(String type, ProceedingJoinPoint pjp, String key) {
        return type + pjp.toString()
                .replace(SPOT, LINE)
                .replace("(", LINE)
                .replace(")", LINE)
                .replace(StringUtils.SPACE, StringUtils.EMPTY)
                .replace(COMMA, LINE).trim() + key;
    }
}
