package com.ark.frigate.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author zengweilong
 * @date 2021/3/30 8:46 PM
 */
@Slf4j
public class RedisLockService {

    private final static String REDIS_LOCK_TYPE = "NX";

    @Autowired
    private JedisUtil jedisUtil;

    public boolean lock(String key, Object value, int seconds) {
        log.info("【Lock Redis key】 = {}, value={}, seconds={}", key, value, seconds);
        return jedisUtil.setValue(key, String.valueOf(value), REDIS_LOCK_TYPE, seconds);
    }

    public boolean unlock(String key) {
        return jedisUtil.delKey(key);
    }

    public String get(String key) {
        return jedisUtil.getValue(key);
    }

    public boolean set(String key, String value, int seconds) {
        log.info("【Cache Redis key】 = {}, value={}, seconds={}", key, value, seconds);
        return jedisUtil.setValue(key, value, seconds);
    }
}
