package com.ark.frigate.redis.lock.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zengweilong
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheManage {

    /**
     * key
     *
     * @return
     */
    String key() default "";

    /**
     * key
     *
     * @return
     */
    int[] args() default {};

    /**
     * 默认有效时间 单位 秒
     *
     * @return
     */
    int timeout() default 300;

}
