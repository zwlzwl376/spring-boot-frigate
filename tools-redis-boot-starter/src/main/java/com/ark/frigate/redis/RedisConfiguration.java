package com.ark.frigate.redis;

import com.ark.frigate.redis.lock.aspect.CacheManageAspect;
import com.ark.frigate.redis.lock.aspect.LockManageAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zengweilong
 * @date 2021/4/5 2:12 PM
 */
@Configuration
@ConditionalOnClass(JedisUtil.class)
public class RedisConfiguration {

    @Bean
    public RedisLockService redisLockService() {
        return new RedisLockService();
    }

    @Bean
    public CacheManageAspect cacheManageAspect() {
        return new CacheManageAspect();
    }

    @Bean
    public LockManageAspect lockManageAspect() {
        return new LockManageAspect();
    }

}
