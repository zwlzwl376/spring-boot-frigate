# 引入Maven 坐标
```xml
	<dependency>
            <artifactId>tools-mybatis-boot-starter</artifactId>
            <groupId>com.ark.frigate</groupId>
            <version>1.0-SNAPSHOT</version>
	</dependency>

```

#主要功能
MyBatis代码生成器

#使用方法

```java

import MyBatisGeneration;
import ConfigHandler;
import DataSourceConfig;
import StrategyConfig;

import java.sql.SQLException;

/**
 * @author zengweilong
 * @date 2021/4/2 2:45 PM
 */
public class MyBatisTools {

    @Test
    public void generationCode() {
        
        MyBatisGeneration generation = new MyBatisGeneration();
        ConfigHandler configHandler = new ConfigHandler();
        
        // 数据源
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDriverClass("com.mysql.cj.jdbc.Driver");
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/db_name");
        dsc.setUsername("root");
        dsc.setPassword("password");
        configHandler.setDataSourceConfig(dsc);
        
        // 生成信息配置
        StrategyConfig sc = new StrategyConfig();
        sc.setAuthor("zengweilong");
        // 实体生成包名 
        sc.setBeanPackage("com.ark.enet.rule.entity");
        // Dao生成包名
        sc.setDaoPackage("com.ark.enet.rule.dao");
        // Mapper文件生成路径
        sc.setXmlPath("/Users/zengweilong/Documents/mybatis");
        // Dao生成的路径
        sc.setDaoPath("/Users/zengweilong/Documents/dao");
        // 实体对应的路径
        sc.setBeanPath("/Users/zengweilong/Documents/entity");
        // 忽略的表
        sc.setIgnoreTable("flyway_schema_history");
        // 包含的表
        sc.setIncludeTable("all");
        // 是否需要使用lombox
        sc.setUseLombok(false);
        configHandler.setStrategyConfig(sc);
        
        // 开始执行 
        generation.getInterfaceExecute(configHandler).start();
    }
}


    
```