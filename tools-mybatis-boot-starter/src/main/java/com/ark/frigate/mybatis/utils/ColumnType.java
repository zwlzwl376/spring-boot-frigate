package com.ark.frigate.mybatis.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * 主要负责与数据库的字段对应
 *
 * @author Along
 */
public class ColumnType {

    private static Logger logger = LoggerFactory.getLogger(ColumnType.class);

    public static final String KEY = "`";

    private static List<String> keyWords = null;

    private static Map<String, String> typeMap = null;

    public static List<String> keyWords() {
        if (null == keyWords) {
            keyWords = Lists.newArrayList("order", "group", "name", "password", "count", "sum", "avg");
        }
        return keyWords;
    }

    public static Map<String, String> typeMap() {
        if (null == typeMap) {
            typeMap = Maps.newHashMap();
            typeMap.put("char", "Character");
            typeMap.put("varchar", "String");
            typeMap.put("date", "java.util.Date");
            typeMap.put("datetime", "java.util.Date");
            typeMap.put("timestamp", "java.sql.Timestamp");
            typeMap.put("tinyint", "Integer");
            typeMap.put("smallint", "Integer");
            typeMap.put("mediumint", "Integer");
            typeMap.put("int", "Integer");
            typeMap.put("integer", "Integer");
            typeMap.put("bigint", "Long");
            typeMap.put("text", "String");
            typeMap.put("bit", "Boolean");
            typeMap.put("decimal", "java.math.BigDecimal");
            typeMap.put("blob", "byte[]");
            typeMap.put("float", "Float");
            typeMap.put("double", "Double");
        }
        return typeMap;
    }

    /**
     * 字段配置
     *
     * @param type 字段类型
     * @return String 映射类型
     */
    public static String processType(String type) {

        if (StringUtils.contains(type, "(")) {
            type = type.substring(0, type.indexOf("("));
        }

        if (StringUtils.isNotBlank(typeMap().get(type))) {
            logger.info("type {} mapping to {}", type, typeMap().get(type));
            return typeMap().get(type);
        }

        for (String key : typeMap().keySet()) {
            if (StringUtils.contains(type, key)) {
                logger.warn("type {} mapping to {}", type, typeMap().get(type));
                return typeMap().get(key);
            }
        }

        logger.error("type {} mapping to {}", type, typeMap().get(type));
        return null;
    }

    /**
     * 将列名转化为Java属性名 2016年11月8日 下午8:41:21
     *
     * @param field 字段名称
     * @return String 类型名称
     */
    public static String processField(String field) {

        StringBuffer sb = new StringBuffer(field.length());
        String[] fields = field.split("_");
        String temp;
        sb.append(fields[0]);
        for (int i = 1; i < fields.length; i++) {
            temp = fields[i].trim();
            sb.append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1));
        }
        return sb.toString();
    }

}
