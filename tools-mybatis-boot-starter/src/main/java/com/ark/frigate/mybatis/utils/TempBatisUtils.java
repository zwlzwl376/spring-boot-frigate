package com.ark.frigate.mybatis.utils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * 主要负责模板读取，模板内容解析，文件生成
 *
 * @author Along
 */
public class TempBatisUtils {


    /**
     * 模板操作
     *
     * @param inputStream 输入流
     * @param paramMap    参数Map
     * @return String 返回内容
     */
    public static String readToTemplate(InputStream inputStream, Map<String, Object> paramMap) {

        String content = FileUtils.readContent(inputStream, StandardCharsets.UTF_8);
        for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
            content = content.replace("${" + entry.getKey() + "}", String.valueOf(entry.getValue()));
        }
        return content;
    }

    /**
     * 通用列子串定义
     *
     * @param columns 列List
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendCol(List<String> columns) {

        StringBuilder strColName = new StringBuilder("");
        int size = columns.size();
        for (int i = 0; i < size; i++) {
            if (ColumnType.keyWords().contains(columns.get(i))) {
                strColName.append(ColumnType.KEY).append(columns.get(i)).append(ColumnType.KEY);
            } else {
                strColName.append(columns.get(i));
            }
            if (i != (size - 1)) {
                strColName.append(",");
            }
        }
        return strColName;
    }

    /**
     * 插入匹配列
     *
     * @param columns 追加列
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendInsertMatchCol(List<String> columns) {

        StringBuilder colName = new StringBuilder("");
        int size = columns.size();
        String tempField = null;
        for (int i = 0; i < size; i++) {
            tempField = ColumnType.processField(columns.get(i));
            colName.append("<if test=\"").append(tempField).append(" != null \"> ");
            if (ColumnType.keyWords().contains(columns.get(i))) {
                colName.append(ColumnType.KEY).append(columns.get(i)).append(ColumnType.KEY);
            } else {
                colName.append(columns.get(i));
            }
            colName.append(",</if>");
            if (i != (size - 1)) {
                colName.append("\n\t\t\t");
            }
        }
        return colName;
    }

    /**
     * 插入匹配值
     *
     * @param columns 匹配列
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendInsertMatchVal(List<String> columns) {

        StringBuilder strVal = new StringBuilder("");
        int size = columns.size();
        String tempField = null;
        for (int i = 0; i < size; i++) {
            tempField = ColumnType.processField(columns.get(i));
            strVal.append("<if test=\"" + tempField + "!=null \"> #{" + tempField + "},</if>");
            if (i != (size - 1)) {
                strVal.append("\n\t\t\t");
            }
        }
        return strVal;
    }

    /**
     * Insert值子串
     *
     * @param columns 列List
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendInsertVal(List<String> columns) {

        StringBuilder strValue = new StringBuilder("");
        int size = columns.size();
        for (int i = 0; i < size; i++) {
            strValue.append("#{" + ColumnType.processField(columns.get(i)) + "}");
            if (i != (size - 1)) {
                strValue.append(",");
            }
        }
        return strValue;
    }

    /**
     * resultMap定义
     *
     * @param columns 列
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendResultMap(List<String> columns) {

        StringBuilder strColName = new StringBuilder("");
        int size = columns.size();
        String tempField = null;
        for (int i = 1; i < size; i++) {
            tempField = ColumnType.processField(columns.get(i));
            strColName.append("<result column=\"" + columns.get(i) + "\" property=\"" + tempField + "\" />");
            if (i != (size - 1)) {
                strColName.append("\n\t\t");
            }
        }
        return strColName;
    }

    /**
     * 更新匹配
     *
     * @param columns 列
     * @return StringBuilder 返回串
     */
    public static StringBuilder appendUpdateMatchSql(List<String> columns) {

        StringBuilder strVal = new StringBuilder("");
        int size = columns.size();
        String tempField = null;
        for (int i = 1; i < size; i++) {
            tempField = ColumnType.processField(columns.get(i));
            strVal.append("<if test=\"").append(tempField).append(" != null \">");
            if (ColumnType.keyWords().contains(columns.get(i))) {
                strVal.append(ColumnType.KEY).append(columns.get(i)).append(ColumnType.KEY);
            } else {
                strVal.append(columns.get(i));
            }
            strVal.append(" = #{").append(tempField).append("},</if>");
            if (i != (size - 1)) {
                strVal.append("\n\t\t\t");
            }
        }
        return strVal;
    }

    /**
     * 更新所有
     *
     * @param columns 列
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendUpdateSql(List<String> columns) {

        StringBuilder strVal = new StringBuilder("");
        int size = columns.size();
        String tempField = null;
        for (int i = 1; i < size; i++) {
            tempField = ColumnType.processField(columns.get(i));
            if (ColumnType.keyWords().contains(columns.get(i))) {
                strVal.append(ColumnType.KEY).append(columns.get(i)).append(ColumnType.KEY);
            } else {
                strVal.append(columns.get(i));
            }
            strVal.append(" = #{").append(tempField).append("}");
            if (i != size - 1) {
                strVal.append(",");
            }
        }
        return strVal;
    }

    /**
     * Where 语句定义 生成
     *
     * @param columns 列
     * @return StringBuilder 返回内容
     */
    public static StringBuilder appendWhere(List<String> columns) {

        StringBuilder strVal = new StringBuilder("");
        int size = columns.size();
        String tempField = null;
        for (int i = 1; i < size; i++) {
            tempField = ColumnType.processField(columns.get(i));
            strVal.append("<if test=\"").append(tempField).append(" != null\"> AND ");
            if (ColumnType.keyWords().contains(columns.get(i))) {
                strVal.append(ColumnType.KEY).append(columns.get(i)).append(ColumnType.KEY);
            } else {
                strVal.append(columns.get(i));
            }
            strVal.append(" = #{").append(tempField).append("}</if>");
            if (i != (size - 1)) {
                strVal.append("\n\t\t\t");
            }
        }
        return strVal;
    }

}
