package com.ark.frigate.mybatis;

import com.ark.frigate.mybatis.common.CommonExecute;
import com.ark.frigate.mybatis.config.ConfigHandler;
import com.ark.frigate.mybatis.dao.BuildDaoExecute;
import com.ark.frigate.mybatis.interfaces.BuildInterfacesExecute;
import com.ark.frigate.mybatis.utils.DbConn;


/**
 * @author zengweilong
 */
public class MyBatisGeneration {

    public CommonExecute getDaoExecute(ConfigHandler configHandler) {
        DbConn.setConfig(configHandler.getDataSourceConfig());
        return new BuildDaoExecute(configHandler);
    }

    public CommonExecute getInterfaceExecute(ConfigHandler configHandler) {
        DbConn.setConfig(configHandler.getDataSourceConfig());
        return new BuildInterfacesExecute(configHandler);
    }

}
