package com.ark.frigate.mybatis.common;

import com.ark.frigate.mybatis.config.ConfigHandler;
import com.ark.frigate.mybatis.config.Constant;
import com.ark.frigate.mybatis.utils.DbConn;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author WeiLong
 */
public abstract class CommonExecute {

    /**
     * 配置
     */
    protected ConfigHandler configHandler;

    /**
     * 开始执行
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public abstract void start() throws ClassNotFoundException, SQLException;

    /**
     * 获取所有的数据库表注释
     *
     * @return Map<String, String> 表注释
     * @throws SQLException           SQL异常
     * @throws ClassNotFoundException 类型异常
     */
    protected Map<String, String> getTableComment() throws SQLException, ClassNotFoundException {
        PreparedStatement prepare = null;
        ResultSet results = null;
        try {
            Map<String, String> maps = Maps.newHashMap();
            prepare = DbConn.getConn().prepareStatement("show table status");
            results = prepare.executeQuery();
            while (results.next()) {
                String tableName = results.getString("NAME");
                String comment = results.getString("COMMENT");
                maps.put(tableName, comment);
            }
            results.close();
            prepare.close();
            return maps;
        } finally {
            if (null != results) {
                results.close();
            }
            if (prepare != null) {
                prepare.close();
            }
        }
    }

    /**
     * 根据表名，获取beanName，MapperName 驼峰处理 2016年11月7日 下午5:20:22
     *
     * @param table 表名
     * @return Map<String, Object> 驼峰处理结果
     */
    protected Map<String, Object> getNamesDao(String table, String typeSuffix) {

        Map<String, Object> resultMap = Maps.newHashMap();
        StringBuffer sb = new StringBuffer(table.length());
        String tableNew = table.toLowerCase();
        String[] tables = tableNew.split("_");
        String temp;
        for (int i = 0; i < tables.length; i++) {
            temp = tables[i].trim();
            sb.append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1));
        }
        resultMap.put(Constant.BEAN_NAME, sb.toString());
        resultMap.put(Constant.DAO_NAME, sb.toString() + typeSuffix);
        resultMap.put(Constant.MAPPER_NAME, sb.toString() + "Mapper");
        return resultMap;
    }

    /**
     * 获取所有的表
     *
     * @return List<String> 表名称List
     * @throws SQLException           SQL异常1
     * @throws ClassNotFoundException 对象类型异常
     */
    protected List<String> getTables() throws SQLException, ClassNotFoundException {
        PreparedStatement prepare = null;
        ResultSet results = null;
        try {
            List<String> tables = Lists.newArrayList();
            prepare = DbConn.getConn().prepareStatement("show tables");
            results = prepare.executeQuery();
            while (results.next()) {
                String tableName = results.getString(1);
                tables.add(tableName);
            }
            results.close();
            prepare.close();
            return tables;
        } finally {
            if (null != results) {
                results.close();
            }
            if (prepare != null) {
                prepare.close();
            }
        }
    }

    public ConfigHandler getConfigHandler() {
        return configHandler;
    }

    public void setConfigHandler(ConfigHandler configHandler) {
        this.configHandler = configHandler;
    }
}
