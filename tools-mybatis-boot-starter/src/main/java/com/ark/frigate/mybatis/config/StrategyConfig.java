package com.ark.frigate.mybatis.config;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zengweilong
 * @date 2021/4/5 12:28 AM
 */
public class StrategyConfig {
    /**
     * 包含哪些表
     */
    private String includeTable = "all";
    /**
     * 排除哪些表
     */
    private String ignoreTable = "no";

    /**
     * 指定生成的路径
     */
    private String beanPath;
    private String daoPath;
    private String xmlPath;

    /**
     * 指定生成的包路径
     */
    private String beanPackage;
    private String daoPackage;

    /**
     * 作者
     */
    private String author;
    /**
     * 可追加的注解
     */
    private String importAnnotation = StringUtils.EMPTY;
    private String writeAnnotation = StringUtils.EMPTY;

    private Boolean useLombok = false;

    public String getIncludeTable() {
        return includeTable;
    }

    public void setIncludeTable(String includeTable) {
        this.includeTable = includeTable;
    }

    public String getIgnoreTable() {
        return ignoreTable;
    }

    public void setIgnoreTable(String ignoreTable) {
        this.ignoreTable = ignoreTable;
    }


    public String getDaoPath() {
        return daoPath;
    }

    public void setDaoPath(String daoPath) {
        this.daoPath = daoPath;
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    public String getBeanPath() {
        return beanPath;
    }

    public void setBeanPath(String beanPath) {
        this.beanPath = beanPath;
    }

    public String getBeanPackage() {
        return beanPackage;
    }

    public void setBeanPackage(String beanPackage) {
        this.beanPackage = beanPackage;
    }

    public String getDaoPackage() {
        return daoPackage;
    }

    public void setDaoPackage(String daoPackage) {
        this.daoPackage = daoPackage;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImportAnnotation() {
        return importAnnotation;
    }

    public void setImportAnnotation(String importAnnotation) {
        this.importAnnotation = importAnnotation;
    }


    public String getWriteAnnotation() {
        return writeAnnotation;
    }

    public void setWriteAnnotation(String writeAnnotation) {
        this.writeAnnotation = writeAnnotation;
    }

    public boolean isUseLombok() {
        return useLombok;
    }

    public void setUseLombok(boolean useLombok) {
        this.useLombok = useLombok;
    }
}
