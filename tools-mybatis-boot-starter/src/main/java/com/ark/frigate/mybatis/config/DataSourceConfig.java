package com.ark.frigate.mybatis.config;

/**
 * 数据库配置
 *
 * @author zengweilong
 * @date 2021/4/5 12:23 AM
 */
public class DataSourceConfig {

    /**
     * 数据库驱动
     */
    private String driverClass;
    /**
     * 数据库用户名
     */
    private String username;
    /**
     * 数据库密码
     */
    private String password;
    /**
     * 数据库连接地址
     */
    private String url;

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
