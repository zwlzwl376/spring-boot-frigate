package com.ark.frigate.mybatis.config;

/**
 * @author zengweilong
 * @date 2021/4/5 12:49 AM
 */
public class ConfigHandler {

    /**
     * 数据源配置
     */
    protected DataSourceConfig dataSourceConfig;

    /**
     * 策略配置
     */
    protected StrategyConfig strategyConfig;


    public DataSourceConfig getDataSourceConfig() {
        return dataSourceConfig;
    }

    public ConfigHandler setDataSourceConfig(DataSourceConfig dataSourceConfig) {
        this.dataSourceConfig = dataSourceConfig;
        return this;
    }

    public StrategyConfig getStrategyConfig() {
        return strategyConfig;
    }

    public ConfigHandler setStrategyConfig(StrategyConfig strategyConfig) {
        this.strategyConfig = strategyConfig;
        return this;
    }
}
