package com.ark.frigate.mybatis.config;

/**
 * 主要负责配置的加载
 *
 * @author Along
 */
public class Constant {

    public final static String BEAN_NAME = "beanName";

    public final static String DAO_NAME = "daoName";

    public final static String MAPPER_NAME = "mapperName";

}
