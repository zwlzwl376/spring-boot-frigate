package com.ark.frigate.zookeeper;

import com.ark.frigate.zookeeper.service.ZookeeperService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.Future;

/**
 * TODO
 *
 * @author zengweilong
 * @date 8/6/21 4:32 PM
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZookeeperApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ZookeeperTest {

    @Autowired
    private ZookeeperService zookeeperService;

    @Test
    public void testThreadLock1() throws Exception {

        List<Future<String>> list = Lists.newArrayList();

        for (int i = 0; i < 10; i++) {
            Future<String> future = zookeeperService.testThreadLock("name ==> " + i);
            list.add(future);
        }

        for (int i = 0; i < list.size(); i++) {
            log.info("========={}", list.get(i).get());
        }
    }

    @Test
    public void testThreadLock2() throws Exception {
        //for (int i = 0; i < 10; i++) {
        zookeeperService.testLock("name ==> " + 1);
        //}
    }

    @Test
    public void testThreadLock3() throws Exception {
        zookeeperService.testLock("name ==> " + 2);
    }

}
