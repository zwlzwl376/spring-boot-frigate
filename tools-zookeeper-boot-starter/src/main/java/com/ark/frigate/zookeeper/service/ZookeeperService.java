package com.ark.frigate.zookeeper.service;

import com.ark.frigate.zookeeper.utils.ZookeeperFairLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

/**
 * TODO
 *
 * @author zengweilong
 * @date 8/6/21 4:37 PM
 */
@Slf4j
@Service
public class ZookeeperService {

    @Async("commonAsyncExecutor")
    public Future<String> testThreadLock(String name) throws Exception {
        String result = testLock(name);
        Future<String> future = new AsyncResult<>(result);
        return future;
    }

    public String testLock(String name) throws Exception {
        log.info("开始执行 = {}", name);
        ZookeeperFairLock zookeeperLock = new ZookeeperFairLock("/lock/test", 1000L);
        if (zookeeperLock.lock()) {
            Thread.sleep(100000L);
            log.info("[获取到Lock] = {}", name);
            zookeeperLock.unLock();
            return name;
        } else {
            log.info("[没有获取到Lock] = ", name);
        }
        return null;
    }
}
