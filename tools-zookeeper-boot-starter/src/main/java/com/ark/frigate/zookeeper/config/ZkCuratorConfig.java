package com.ark.frigate.zookeeper.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * TODO
 *
 * @author zengweilong
 * @date 8/6/21 3:43 PM
 */
@Slf4j
@Configuration
public class ZkCuratorConfig {

    @Value("${spring.zookeeper.address}")
    private String connectString;

    @Value("${spring.zookeeper.timeout}")
    private int timeout;

    @Bean("curatorFramework")
    public CuratorFramework curatorFramework() {
        log.info("ZK CuratorFramework ===========> init start.");
        RetryPolicy policy = new ExponentialBackoffRetry(1000, 10);
        // 通过工厂创建Curator
        CuratorFramework curator = CuratorFrameworkFactory.builder().connectString(connectString)
                .sessionTimeoutMs(timeout).retryPolicy(policy).build();
        curator.start();
        log.info("ZK CuratorFramework ===========> init complete.");
        return curator;
    }
}
