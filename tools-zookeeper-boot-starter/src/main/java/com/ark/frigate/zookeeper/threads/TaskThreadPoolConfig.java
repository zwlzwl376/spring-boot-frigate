package com.ark.frigate.zookeeper.threads;

/**
 * @author zengweilong
 */
public class TaskThreadPoolConfig {

    public final static int CPU_SIZE = Runtime.getRuntime().availableProcessors();

    public final static int CORE_POOL_SIZE = CPU_SIZE;

    public final static int MAX_POOL_SIZE = 1000;

    public static final int QUEUE_CAPACITY = 200;

}
