package com.ark.frigate.dingtalk.ding.message;

import com.ark.frigate.dingtalk.ding.MsgType;
import com.ark.frigate.dingtalk.utils.DingTalkMessageJson;
import lombok.Data;

/**
 * {
 * "msgtype": "text",
 * "text": {
 * "content": "我就是我, 是不一样的烟火@156xxxx8827"
 * },
 * "at": {
 * "atMobiles": [
 * "156xxxx8827",
 * "189xxxx8325"
 * ],
 * "isAtAll": false
 * }
 * }
 */
@Data
public class TextMessage extends AbstractMessage {

    private String content;

    private String[] atMobiles;

    @Override
    public String toJson() {
        TextFormat textBean = new TextFormat();
        textBean.setMsgtype(MsgType.text);

        Message message = new Message();
        message.setContent(content);
        textBean.setText(message);

        Mobiles mobiles = new Mobiles();
        mobiles.setIsAtAll(true);
        mobiles.setAtMobiles(atMobiles);
        textBean.setAt(mobiles);

        return DingTalkMessageJson.toJson(textBean);
    }

    @Data
    class TextFormat {

        private MsgType msgtype;

        private Message text;

        private Mobiles at;
    }

    @Data
    class Message {

        private String content;

    }

}
