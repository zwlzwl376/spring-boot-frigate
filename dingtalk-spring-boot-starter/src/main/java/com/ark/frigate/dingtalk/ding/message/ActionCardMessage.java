package com.ark.frigate.dingtalk.ding.message;

import com.ark.frigate.dingtalk.ding.MsgType;
import com.ark.frigate.dingtalk.ding.message.bean.ActionCard;
import com.ark.frigate.dingtalk.utils.DingTalkMessageJson;
import lombok.Data;

@Data
public class ActionCardMessage extends AbstractMessage {

    private ActionCard actionCard;

    @Override
    public String toJson() {
        ActionCardFormat actionCardBean = new ActionCardFormat();
        actionCardBean.setMsgtype(MsgType.actionCard);
        actionCardBean.setActionCard(actionCard);
        return DingTalkMessageJson.toJson(actionCardBean);
    }

    @Data
    class ActionCardFormat {

        private MsgType msgtype;

        private ActionCard actionCard;

    }
}
