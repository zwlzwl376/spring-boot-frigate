package com.ark.frigate.dingtalk.ding.message;

import com.ark.frigate.dingtalk.ding.MsgType;
import com.ark.frigate.dingtalk.ding.message.bean.CardLink;
import com.ark.frigate.dingtalk.utils.DingTalkMessageJson;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * {
 * "feedCard": {
 * "links": [
 * {
 * "title": "时代的火车向前开",
 * "messageURL": "https://www.dingtalk.com/s?__biz=MzA4NjMwMTA2Ng==&mid=2650316842&idx=1&sn=60da3ea2b29f1dcc43a7c8e4a7c97a16&scene=2&srcid=09189AnRJEdIiWVaKltFzNTw&from=timeline&isappinstalled=0&key=&ascene=2&uin=&devicetype=android-23&version=26031933&nettype=WIFI",
 * "picURL": "https://www.dingtalk.com/"
 * },
 * {
 * "title": "时代的火车向前开2",
 * "messageURL": "https://www.dingtalk.com/s?__biz=MzA4NjMwMTA2Ng==&mid=2650316842&idx=1&sn=60da3ea2b29f1dcc43a7c8e4a7c97a16&scene=2&srcid=09189AnRJEdIiWVaKltFzNTw&from=timeline&isappinstalled=0&key=&ascene=2&uin=&devicetype=android-23&version=26031933&nettype=WIFI",
 * "picURL": "https://www.dingtalk.com/"
 * }
 * ]
 * },
 * "msgtype": "feedCard"
 * }
 */
@Data
public class FeedCardMessage extends AbstractMessage {


    public List<CardLink> links;


    @Override
    public String toJson() {
        FeedCardFormat feedCardBean = new FeedCardFormat();
        feedCardBean.setMsgtype(MsgType.feedCard);
        FeedCard feedCard = new FeedCard();
        feedCard.setLinks(links);
        feedCardBean.setFeedCard(feedCard);
        return DingTalkMessageJson.toJson(feedCardBean);
    }

    public Boolean putLinks(String title, String messageURL, String picURL) {
        if (CollectionUtils.isEmpty(links)) {
            links = new ArrayList<>();
        }
        CardLink cardLink = new CardLink();
        cardLink.setTitle(title);
        cardLink.setMessageURL(messageURL);
        cardLink.setPicURL(picURL);
        links.add(cardLink);
        return true;
    }

    @Data
    class FeedCardFormat {

        private MsgType msgtype;

        private FeedCard feedCard;
    }

    @Data
    class FeedCard {

        private List<CardLink> links;
    }

}
