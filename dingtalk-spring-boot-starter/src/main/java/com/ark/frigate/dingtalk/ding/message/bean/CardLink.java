package com.ark.frigate.dingtalk.ding.message.bean;

import lombok.Data;


@Data
public class CardLink {

    private String title;

    private String messageURL;

    private String picURL;
}