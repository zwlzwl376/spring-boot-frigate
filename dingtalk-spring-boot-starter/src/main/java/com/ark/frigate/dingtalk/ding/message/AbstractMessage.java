package com.ark.frigate.dingtalk.ding.message;

import lombok.Data;

@Data
public abstract class AbstractMessage {

    private String dingTalkUrl;

    public abstract String toJson();


    @Data
    class Mobiles {

        private String[] atMobiles;

        private Boolean isAtAll;
    }
}
