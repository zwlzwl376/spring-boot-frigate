package com.ark.frigate.dingtalk.ding.message;

import com.ark.frigate.dingtalk.ding.MsgType;
import com.ark.frigate.dingtalk.utils.DingTalkMessageJson;
import lombok.Data;
import org.apache.commons.lang3.ArrayUtils;


@Data
public class MarkDownMessage extends AbstractMessage {

    private MarkDown markDown;

    private String[] atMobiles;

    @Override
    public String toJson() {
        MarkDownFormat markDownFormat = new MarkDownFormat();
        markDownFormat.setMsgtype(MsgType.markdown);

        Mobiles mobiles = new Mobiles();
        mobiles.setIsAtAll(true);
        if (ArrayUtils.isNotEmpty(atMobiles)) {
            mobiles.setAtMobiles(atMobiles);
        }
        markDownFormat.setAt(mobiles);

        markDownFormat.setMarkDown(markDown);

        return DingTalkMessageJson.toJson(markDownFormat);
    }

    public Boolean setMarkDown(String title, String text) {
        if (markDown == null) {
            markDown = new MarkDown();
        }
        markDown.setTitle(title);
        markDown.setText(text);
        return true;
    }

    @Data
    class MarkDownFormat {

        private MsgType msgtype;

        private MarkDown markDown;

        private Mobiles at;
    }

    @Data
    class MarkDown {

        private String title;

        private String text;
    }
}
