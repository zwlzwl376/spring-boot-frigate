package com.ark.frigate.dingtalk.ding;

public enum MsgType {

    text,

    link,

    feedCard,

    markdown,

    actionCard
}
