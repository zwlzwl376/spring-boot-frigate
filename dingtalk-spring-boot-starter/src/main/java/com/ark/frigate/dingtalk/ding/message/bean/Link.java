package com.ark.frigate.dingtalk.ding.message.bean;

import lombok.Data;

@Data
public class Link {

    private String text;

    private String title;

    private String picUrl;

    private String messageUrl;
}
