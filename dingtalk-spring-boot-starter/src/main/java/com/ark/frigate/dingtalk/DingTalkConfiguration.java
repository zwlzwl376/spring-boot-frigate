package com.ark.frigate.dingtalk;

import com.ark.frigate.dingtalk.ding.DingTalkMessage;
import com.ark.frigate.dingtalk.ding.DingTalkTextMessage;
import com.ark.frigate.dingtalk.properties.DingTalkConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author zengweilong
 */
@Configuration
@ConditionalOnWebApplication
@EnableConfigurationProperties({DingTalkConfig.class})
public class DingTalkConfiguration {

    @Bean
    public DingTalkMessage dingTalkMessage() {
        return new DingTalkMessage();
    }

    @Bean
    public DingTalkTextMessage dingTalkTextMessage() {
        return new DingTalkTextMessage();
    }


    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
