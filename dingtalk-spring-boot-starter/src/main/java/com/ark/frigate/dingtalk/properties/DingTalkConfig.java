package com.ark.frigate.dingtalk.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zengweilong
 */
@Data
@ConfigurationProperties("ding.talk")
public class DingTalkConfig {

    /**
     * 签名
     */
    private String secret;

    /**
     * 默认报警地址
     */
    private String url;

    /**
     * 信息@人
     */
    private String phones;

    /**
     * 开关 默认打开
     */
    private Boolean enable = true;
}
