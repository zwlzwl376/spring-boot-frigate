package com.ark.frigate.dingtalk.ding;

import com.ark.frigate.dingtalk.ding.message.TextMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 异常报警
 * <p>
 * https://developers.dingtalk.com/document/app/custom-robot-access
 *
 * @author zengweilong
 */
@Slf4j
public class DingTalkTextMessage extends DingTalkMessage {

    public void sendTextEx(Throwable e) {
        sendCoreExMessage(StringUtils.EMPTY, e, StringUtils.EMPTY, ArrayUtils.EMPTY_STRING_ARRAY);
    }

    public void sendTextExMsg(Throwable e, String message) {
        sendCoreExMessage(StringUtils.EMPTY, e, message, ArrayUtils.EMPTY_STRING_ARRAY);
    }

    public void sendTextEx(String dingTalkUrl, Throwable e) {
        sendCoreExMessage(dingTalkUrl, e, StringUtils.EMPTY, ArrayUtils.EMPTY_STRING_ARRAY);
    }

    public void sendTextEx(Throwable e, String[] phones) {
        sendCoreExMessage(StringUtils.EMPTY, e, StringUtils.EMPTY, phones);
    }

    public void sendTextMsg(String dingTalkUrl, Throwable e, String message) {
        sendCoreExMessage(dingTalkUrl, e, message, ArrayUtils.EMPTY_STRING_ARRAY);
    }


    public void sendText(String message) {
        sendCoreTextMessage(StringUtils.EMPTY, message, ArrayUtils.EMPTY_STRING_ARRAY);
    }


    public void sendText(String dingTalkUrl, String message) {
        sendCoreTextMessage(dingTalkUrl, message, ArrayUtils.EMPTY_STRING_ARRAY);
    }

    /**
     * 发送带异常栈的钉钉消息
     *
     * @param dingTalkUrl
     * @param e
     * @param message
     * @param atMobiles
     */
    public void sendCoreExMessage(String dingTalkUrl, Throwable e, String message, String[] atMobiles) {
        String coveContent = getExtMsg(e, message);
        TextMessage textMessage = new TextMessage();
        textMessage.setDingTalkUrl(dingTalkUrl);
        textMessage.setContent(coveContent);

        if (ArrayUtils.isEmpty(atMobiles)) {
            atMobiles = StringUtils.split(dingTalkConfig.getPhones(), ",");
        }

        textMessage.setAtMobiles(atMobiles);
        try {
            sendCoreMessage(textMessage);
        } catch (Exception ex) {
            log.error("钉消息发送失败={}", textMessage, ex);
        }
    }

    /**
     * 发送文本消息
     *
     * @param dingTalkUrl
     * @param message
     * @param atMobiles
     */
    public void sendCoreTextMessage(String dingTalkUrl, String message, String[] atMobiles) {
        StringBuilder textBuilder = new StringBuilder(getEnv() + getHostName())
                .append(" @[").append(getHostIp()).append("]")
                .append(StringUtils.LF)
                .append(message);
        TextMessage textMessage = new TextMessage();
        textMessage.setDingTalkUrl(dingTalkUrl);
        textMessage.setContent(textBuilder.toString());
        textMessage.setAtMobiles(atMobiles);
        try {
            sendCoreMessage(textMessage);
        } catch (Exception ex) {
            log.error("钉消息发送失败={}", textMessage, ex);
        }
    }

    /**
     * Throwable 解析
     *
     * @param e
     * @param message
     * @return
     */
    private String getExtMsg(Throwable e, String message) {

        if (null == e) {
            return message;
        }

        StringBuilder textBuilder = new StringBuilder(getEnv() + getHostName())
                .append(" @[")
                .append(getHostIp()).append("]").append(StringUtils.LF)
                .append(e.getClass().getName()).append(StringUtils.LF)
                .append(e.getMessage()).append(StringUtils.LF);

        if (StringUtils.isNotBlank(message)) {
            textBuilder.append(message).append(StringUtils.LF);
        }
        StackTraceElement[] elements = e.getStackTrace();
        for (StackTraceElement element : elements) {
            textBuilder.append(element).append(StringUtils.LF);
        }
        String contents = textBuilder.toString();
        return contents;
    }


}
