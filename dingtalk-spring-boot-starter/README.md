# 引入Maven 坐标
```xml
	<dependency>
            <groupId>com.ark.dingtalk</groupId>
            <artifactId>dingtalk-spring-boot-starter</artifactId>
            <version>1.0-SNAPSHOT</version>
	</dependency>

```

# 主要功能

向钉钉群发送消息

# 配置 application.properties
```properties

ding.talk.secret=               // 钉钉私钥
ding.talk.url=                  // 钉消息请求URL
ding.talk.phones=18510337285    // 默认@的人
ding.talk.enable= true | false  // 消息开关
```

例如：application.yml

```yaml
ding:
  talk:
    url: https://oapi.dingtalk.com/robot/send?access_token=e3c6fe81859c4e6f0927f840ecae0d883b85b7657b55a12f7874583b62695bd1&timestamp=%s&sign=%s
    secret: SECb1eda4ea99da743f4238a1ce3f1ecf4b426196b9c80ab28f8fc0306c2275224d
    phones: 18510337285
    enable: true
 
```


#使用方法

```java
@Service
public class MyService {
    /**
     *  支持文本
     */
    @Autowired
    DingTalkTextMessage dingTalkTextMessage;
    /**
     *  支持图片 链接
     */
    @Autowired
    DingTalkMessage dingTalkMessage;

    public void myMethod() {
        //文本
        dingTalkTextMessage.sendText("文本");

        //文本 + 异常
        dingTalkTextMessage.sendTextExMsg(e, "文本");

        //异常
        dingTalkTextMessage.sendTextEx(e);

    }
    
}

```

