package com.ark.frigate.shell.test.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * TODO
 *
 * @author zengweilong
 * @date 8/5/21 3:20 PM
 */
public class LockTest {

    private Lock lock = new ReentrantLock();

    public void add(String name) {
        lock.lock();
        try {
            Thread.sleep(1000);
            System.out.println(" ==== " + name);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            lock.unlock();
        }

    }
}
