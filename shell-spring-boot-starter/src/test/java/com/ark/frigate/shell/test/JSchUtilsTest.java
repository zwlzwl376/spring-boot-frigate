package com.ark.frigate.shell.test;

import com.ark.frigate.shell.utils.JSchUtils;
import com.ark.frigate.shell.utils.SocketUtils;
import com.ark.frigate.shell.utils.dto.ConfigPo;
import com.ark.frigate.shell.utils.dto.ResultDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author zengweilong
 * @date 5/31/21 3:00 PM
 */
@Slf4j
public class JSchUtilsTest {

    @Test
    public void testCommand() {
        ConfigPo configPo = ConfigPo.builder().host("172.16.71.103").username("root").password("123").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        ResultDto resultDto = null;
        if (result) {
            resultDto = jSch.execCommand("mkdir -p /home/dap3/zwl-zip");
            log.info("result = {}", resultDto);
        } else {
            log.error("Connection fail");
            return;
        }

        if (resultDto.success()) {
            resultDto = jSch.execCommand("rm -rf /home/auto/workspace/123");
            log.info("result = {}", resultDto);
        }
        jSch.closeSession();
    }

    /**
     * 文件传输，root已经设置了免密登陆ssh_key验证
     * 103 传输到 102
     */
    @Test
    public void testScp() {
        ConfigPo configPo = ConfigPo.builder().host("172.16.71.103").username("root").password("123").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        if (result) {
            ResultDto resultDto = jSch.execCommand("scp /home/apache-maven-3.5.4-bin.tar.gz root@172.16.71.102:/home/test/workspace/apache-maven-3.5.4-bin.tar.gz");
            jSch.closeSession();
            log.info("result 1 = {}", resultDto);

            configPo.setHost("172.16.71.102");
            jSch = JSchUtils.builder().configPo(configPo).build();
            jSch.connect();
            resultDto = jSch.execCommand("rm -rf /home/test/workspace/apache-maven-3.5.4-bin.tar.gz");
            jSch.closeSession();
            log.info("result 2 = {}", resultDto);
        } else {
            log.error("Connection fail");
        }
        jSch.closeSession();
    }


    @Test
    public void testScpPwd() {
        ConfigPo configPo = ConfigPo.builder().host("172.16.71.103").username("test").password("123").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        if (result) {
            ResultDto resultDto = jSch.execCommand("scp /home/apache-maven-3.5.4-bin.tar.gz test@172.16.71.102:/home/test/workspace/");
            log.info("result = {}", resultDto);
        } else {
            log.error("Connection fail");
        }
        jSch.closeSession();
    }

    /**
     * Shell 命令测试
     */
    @Test
    public void testShell() {
        ConfigPo configPo = ConfigPo.builder().host("172.16.71.102").username("test").password("123").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        if (result) {
            jSch.execCommand("rm -rf /home/test/workspace/china");
            ResultDto resultDto = jSch.executeShell("cd /home/test/workspace/; mkdir china; exit;");
            log.info("result = {}", resultDto.getResult());
        } else {
            log.error("Connection fail");
        }
        jSch.closeSession();
    }

    /**
     * 判断端口是否打开
     */
    @Test
    public void testSocketPort() {
        System.out.println(SocketUtils.testPort("36.137.32.185", 18081));
    }

    /**
     * Sftp本地文件上传
     *
     * @throws Exception
     */
    @Test
    public void testSFtp() throws Exception {
        ConfigPo configPo = ConfigPo.builder().host("10.114.14.89").username("ecas_flow").password("ecas@flow").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        if (result) {
            File file = new File("/Users/zengweilong/Downloads/flow-service-lib-202107302012.zip");
            InputStream is = new FileInputStream(file);
            String directory = "/home/ecas_flow/";
            ResultDto resultDto = jSch.sftpUpload(directory, "flow-service-lib-202107302012.zip", is);
            log.info("result = {}", resultDto);
            is.close();
        } else {
            log.error("Connection fail");
        }
        jSch.closeSession();
    }


    /**
     * Sftp本地文件上传
     *
     * @throws Exception
     */
    @Test
    public void testSFtp2() throws Exception {
        ConfigPo configPo = ConfigPo.builder().host("10.114.10.129").username("ecas_flow").password("pt@Flow").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        if (result) {
            File file = new File("/Users/zengweilong/Downloads/ecas-ecas-service-api-1.0.0-20210731.082205-899.jar");
            InputStream is = new FileInputStream(file);
            String directory = "/home/ecas_flow/";
            ResultDto resultDto = jSch.sftpUpload(directory, "ecas-ecas-service-api-1.0.0-20210731.082205-899.jar", is);
            log.info("result = {}", resultDto);
            is.close();
        } else {
            log.error("Connection fail");
        }
        jSch.closeSession();
    }

}
