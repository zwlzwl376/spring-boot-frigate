package com.ark.frigate.shell.test.thread;

/**
 * TODO
 *
 * @author zengweilong
 * @date 8/5/21 3:22 PM
 */
public class MoreClass extends Thread {

    private LockTest lockTest;

    private String name;

    public MoreClass(LockTest lockTest, String name) {
        this.lockTest = lockTest;
        this.name = name;
    }


    @Override
    public void run() {
        lockTest.add(name);
    }
}
