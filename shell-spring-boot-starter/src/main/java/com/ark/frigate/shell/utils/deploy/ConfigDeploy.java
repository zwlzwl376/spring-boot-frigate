package com.ark.frigate.shell.utils.deploy;

import com.ark.frigate.shell.utils.deploy.machine.MachineInfo;
import com.ark.frigate.shell.utils.deploy.module.config.ConfigCodeDeployImpl;
import com.ark.frigate.shell.utils.deploy.module.config.ConfigWebDeployImpl;
import com.ark.frigate.shell.utils.deploy.po.MachinePo;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.List;

/**
 * TODO
 *
 * @author zengweilong
 * @date 7/15/21 3:28 PM
 */
@Slf4j
public class ConfigDeploy {


    /**
     * 发布调用
     */
    public static void main(String[] args) {
        // 备份核心名称
        String yMdHmsName = "config_%s_" + DateFormatUtils.format(System.currentTimeMillis(), "MMddHHmmss");
        // 部署前端
        //deployWeb(yMdHmsName);
        // 部署后端
        deployCode(yMdHmsName);
    }


    private static void deployWeb(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/dap-web/config-web/page/module/";
        //所属模块
        String moduleName = "config";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "html");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/DAP-WEB/dap-web/config-web/page/module/config.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        list.add(MachineInfo.get86(bakNamePix, workspace, moduleName));

        /*
        // 89 跳批
        list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));
        */

        // 103 本地
        //list.add(MachineInfo.get103(bakName, workspace, moduleName));

        ConfigWebDeployImpl webDeploy = new ConfigWebDeployImpl();
        webDeploy.deployProcess(list, bakNamePix, srcFile, false);
    }

    private static void deployCode(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/config-server/";
        //所属模块
        String moduleName = "dap-boot-starter-config-server-1.0-SNAPSHOT";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "java");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/CONFIG-SERVER/dap-boot-starter-config-server-1.0-SNAPSHOT.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        //list.add(MachineInfo.get86(bakNamePix, "/home/dap3/dap-core/", moduleName));

        // 89 跳批
        //list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        //list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));


        // 103 本地
        list.add(MachineInfo.get103(bakNamePix, "/home/test/workspace/", moduleName));

        ConfigCodeDeployImpl codeDeploy = new ConfigCodeDeployImpl();
        codeDeploy.deployProcess(list, bakNamePix, srcFile, true);
    }
}
