package com.ark.frigate.shell.utils.deploy;

import com.ark.frigate.shell.utils.deploy.machine.MachineInfo;
import com.ark.frigate.shell.utils.deploy.module.sequence.design.SequenceDesignDeployImpl;
import com.ark.frigate.shell.utils.deploy.module.sequence.design.SequenceDesignWebDeployImpl;
import com.ark.frigate.shell.utils.deploy.module.sequence.server.SequenceServerDeployImpl;
import com.ark.frigate.shell.utils.deploy.po.MachinePo;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.List;

/**
 * TODO
 *
 * @author zengweilong
 * @date 7/28/21 9:07 PM
 */
public class SequenceDesignDeploy {


    /**
     * 发布调用
     */
    public static void main(String[] args) {
        // 备份核心名称
        String yMdHmsName = "sequence_design_%s_" + DateFormatUtils.format(System.currentTimeMillis(), "MMddHHmmss");
        // 部署前端
        //deployWeb(yMdHmsName);
        // 部署后端
        deployCode(yMdHmsName);
        // 部署服务
        //deployServer(yMdHmsName);
    }


    private static void deployWeb(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/dap-web/config-web/page/module/";
        //所属模块
        String moduleName = "sequence";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "html");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/DAP-WEB/dap-web/config-web/page/module/sequence.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        list.add(MachineInfo.get86(bakNamePix, workspace, moduleName));

        /*
        // 89 跳批
        list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));
        */

        // 103 本地
        //list.add(MachineInfo.get103(bakName, workspace, moduleName));

        SequenceDesignWebDeployImpl webDeploy = new SequenceDesignWebDeployImpl();
        webDeploy.deployProcess(list, bakNamePix, srcFile, false);
    }

    private static void deployCode(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/dap-sequence/";
        //所属模块
        String moduleName = "dap-boot-starter-sequence-design-1.0-SNAPSHOT";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "java");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/SEQUENCE-DESIGN/dap-boot-starter-sequence-design-1.0-SNAPSHOT.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        list.add(MachineInfo.get86(bakNamePix, workspace, moduleName));

        // 89 跳批
        //list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        //list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));


        // 103 本地
        //list.add(MachineInfo.get103(bakNamePix, workspace, moduleName));

        SequenceDesignDeployImpl codeDeploy = new SequenceDesignDeployImpl();
        codeDeploy.deployProcess(list, bakNamePix, srcFile, true);
    }


    private static void deployServer(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/dap-sequence/";
        //所属模块
        String moduleName = "dap-boot-starter-sequence-server-1.0-SNAPSHOT";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "server");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/SEQUENCE-SERVER/dap-boot-starter-sequence-server-1.0-SNAPSHOT.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        list.add(MachineInfo.get86(bakNamePix, workspace, moduleName));

        // 89 跳批
        //list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        //list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));

        // 103 本地
        //list.add(MachineInfo.get103(bakNamePix, workspace, moduleName));
        SequenceServerDeployImpl serverDeploy = new SequenceServerDeployImpl();
        serverDeploy.deployProcess(list, bakNamePix, srcFile, true);
    }
}
