package com.ark.frigate.shell.utils.deploy.po;

import lombok.Data;

/**
 * TODO
 *
 * @author zengweilong
 * @date 7/15/21 2:37 PM
 */
@Data
public class MachinePo {
    /**
     * /home/dap3/dap-cache/
     * 工作空间
     */
    private String workspace;

    /**
     * IP
     */
    private String host;

    /**
     * 例：abc
     * 用户
     */
    private String username;

    /**
     * 例如：123
     * 密码
     */
    private String password;

    /**
     * 例：22
     * 端口
     */
    private Integer port;

    /**
     * 例：0706124312_cache_new.zip
     * 文件上传到服务器后的压缩包名字（包含后缀），改名字会用于服务器解压缩
     */
    private String fileName;

    /**
     * 例：cache-server-1.0-SNAPSHOT
     * 需要备份的目录，该目录也是项目目录，执行覆盖前会将其压缩备份。上传的文件会按照要求进行覆盖
     * 具体覆盖流程可以自行扩展
     */
    private String backPath;
}
