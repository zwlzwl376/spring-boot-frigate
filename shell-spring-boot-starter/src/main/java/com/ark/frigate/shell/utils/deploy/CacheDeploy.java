package com.ark.frigate.shell.utils.deploy;

import com.ark.frigate.shell.utils.deploy.machine.MachineInfo;
import com.ark.frigate.shell.utils.deploy.module.cache.CacheCodeDeployImpl;
import com.ark.frigate.shell.utils.deploy.module.cache.CacheWebDeployImpl;
import com.ark.frigate.shell.utils.deploy.po.MachinePo;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.List;

/**
 * TODO
 *
 * @author zengweilong
 * @date 7/15/21 3:28 PM
 */
@Slf4j
public class CacheDeploy {


    /**
     * 缓存发布调用
     */
    public static void main(String[] args) {
        // 备份核心名称
        String yMdHmsName = "cache_%s_" + DateFormatUtils.format(System.currentTimeMillis(), "MMddHHmmss");
        // 部署前端
        //deployWeb(yMdHmsName);
        // 部署后端
        deployCode(yMdHmsName);
    }


    private static void deployWeb(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/dap-web/config-web/page/module/";
        //所属模块
        String moduleName = "cache";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "html");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/DAP-WEB/dap-web/config-web/page/module/cache.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        list.add(MachineInfo.get86(bakNamePix, workspace, moduleName));


        // 89 跳批
        //list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));

        // 103 本地
        //list.add(MachineInfo.get103(bakNamePix, workspace, moduleName));

        CacheWebDeployImpl cacheDeploy = new CacheWebDeployImpl();
        cacheDeploy.deployProcess(list, bakNamePix, srcFile, false);
    }

    private static void deployCode(String yMdHmsName) {
        // 工作空间
        String workspace = "/home/dap3/dap-cache/";
        //所属模块
        String moduleName = "cache-server-1.0-SNAPSHOT";
        // 备份文件名前缀
        String bakNamePix = String.format(yMdHmsName, "java");
        // 本机上需要上传的zip包
        String srcFile = "/Users/zengweilong/library-gientech/svn-dap/svn-cornstone/CACHE-SERVER/cache-server-1.0-SNAPSHOT.zip";

        List<MachinePo> list = Lists.newArrayList();

        // 86 开发环境
        list.add(MachineInfo.get86(bakNamePix, workspace, moduleName));

        // 89 跳批
        //list.add(MachineInfo.get89(bakNamePix, workspace, moduleName));

        // 130 SIT
        list.add(MachineInfo.get130(bakNamePix, workspace, moduleName));

        // 103 本地
        //list.add(MachineInfo.get103(bakNamePix, workspace, moduleName));

        CacheCodeDeployImpl cacheDeploy = new CacheCodeDeployImpl();
        cacheDeploy.deployProcess(list, bakNamePix, srcFile, true);
    }
}
