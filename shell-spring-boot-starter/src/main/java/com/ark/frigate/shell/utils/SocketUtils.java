package com.ark.frigate.shell.utils;

import lombok.extern.slf4j.Slf4j;

import java.net.Socket;

/**
 * Socket
 *
 * @author zengweilong
 * @date 5/31/21 2:31 PM
 */
@Slf4j
public class SocketUtils {

    public static boolean testPort(String host, Integer port) {
        Socket client;
        try {
            client = new Socket(host, port);
            client.close();
            return true;
        } catch (Exception e) {
            log.error("Socket Connect fail. host={}, port={}", host, port, e);
            return false;
        }
    }
}
