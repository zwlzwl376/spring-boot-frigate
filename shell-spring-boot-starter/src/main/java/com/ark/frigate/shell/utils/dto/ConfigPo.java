package com.ark.frigate.shell.utils.dto;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * TODO
 *
 * @author zengweilong
 * @date 6/2/21 6:01 PM
 */
@Slf4j
@Data
@Builder
public class ConfigPo {

    private String host;

    private Integer port;

    private String username;

    private String password;

    private String identity;
}
