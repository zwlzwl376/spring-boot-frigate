package com.ark.frigate.shell.utils.dto;

import lombok.Data;

/**
 * TODO
 *
 * @author zengweilong
 * @date 5/31/21 3:20 PM
 */
@Data
public class ResultDto {

    private Integer code = -2;

    private String result;

    public boolean success() {
        return 0 == code;
    }

}
