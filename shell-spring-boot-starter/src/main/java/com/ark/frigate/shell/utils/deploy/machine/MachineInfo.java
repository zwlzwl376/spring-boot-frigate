package com.ark.frigate.shell.utils.deploy.machine;

import com.ark.frigate.shell.utils.deploy.po.MachinePo;

/**
 * TODO
 *
 * @author zengweilong
 * @date 7/16/21 5:32 PM
 */
public class MachineInfo {

    public static MachinePo get130(String fileName, String workSpace, String moduleName) {
        MachinePo infoPo130 = new MachinePo();
        infoPo130.setHost("10.114.10.130");
        infoPo130.setUsername("dap3");
        infoPo130.setPassword("dap@123");
        infoPo130.setPort(22);
        infoPo130.setWorkspace(workSpace);
        infoPo130.setFileName(fileName + "_new.zip");
        infoPo130.setBackPath(moduleName);
        return infoPo130;
    }

    public static MachinePo get89(String fileName, String workSpace, String moduleName) {
        MachinePo infoPo89 = new MachinePo();
        infoPo89.setHost("10.114.14.89");
        infoPo89.setUsername("dap3");
        infoPo89.setPassword("ecas@dap3");
        infoPo89.setPort(22);
        infoPo89.setWorkspace(workSpace);
        infoPo89.setFileName(fileName + "_new.zip");
        infoPo89.setBackPath(moduleName);
        return infoPo89;
    }

    public static MachinePo get103(String fileName, String workSpace, String moduleName) {
        MachinePo infoPo103 = new MachinePo();
        infoPo103.setHost("172.16.71.103");
        infoPo103.setUsername("root");
        infoPo103.setPassword("123");
        infoPo103.setPort(22);
        infoPo103.setWorkspace(workSpace);
        infoPo103.setFileName(fileName + "_new.zip");
        infoPo103.setBackPath(moduleName);
        return infoPo103;
    }

    public static MachinePo get86(String fileName, String workSpace, String moduleName) {
        MachinePo infoPo86 = new MachinePo();
        infoPo86.setHost("36.110.19.86");
        infoPo86.setUsername("dap3");
        infoPo86.setPassword("Pac@dap3#2020");
        infoPo86.setPort(4482);
        infoPo86.setWorkspace(workSpace);
        infoPo86.setFileName(fileName + "_new.zip");
        infoPo86.setBackPath(moduleName);
        return infoPo86;
    }
}
