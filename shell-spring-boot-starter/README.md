# 引入Maven 坐标
```xml
	<dependency>
            <groupId>com.ark.frigate</groupId>
            <artifactId>shell-spring-boot-starter</artifactId>
            <version>1.0-SNAPSHOT</version>
	</dependency>

```

#主要功能
Shell 服务器远程命令执行工具

#使用方法

```java

public class JSchUtilsTest {

    @Test
    public void testCommand() {
        ConfigPo configPo = ConfigPo.builder().host("172.16.71.103").username("root").password("123").port(22).build();
        JSchUtils jSch = JSchUtils.builder().configPo(configPo).build();
        boolean result = jSch.connect();
        ResultDto resultDto = null;
        // 创建目录
        if (result) {
            resultDto = jSch.execCommand("mkdir -p /home/dap3/zwl-zip");
            log.info("result = {}", resultDto);
        } else {
            log.error("Connection fail");
            return;
        }

        // 删除目录
        if (resultDto.success()) {
            resultDto = jSch.execCommand("rm -rf /home/auto/workspace/123");
            log.info("result = {}", resultDto);
        }
        jSch.closeSession();
    }
}
    
```