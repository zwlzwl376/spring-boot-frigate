package com.ark.frigate.spring.handler;

import com.ark.frigate.spring.MessageDto;

/**
 * @author zengweilong
 * @date 4/14/21 2:33 PM
 */
public interface MessageHandler {
    /**
     * 通知执行方
     *
     * @param messageDto
     */
    void publishMessage(MessageDto messageDto);
}
