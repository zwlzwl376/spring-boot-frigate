package com.ark.frigate.spring;

import com.ark.frigate.spring.listerner.EventMessageListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zengweilong
 * @date 4/11/21 4:49 PM
 */
@Configuration
@ConditionalOnWebApplication
public class EventMessageConfiguration {

    @Bean
    public EventMessageListener eventMessageListener() {
        return new EventMessageListener();
    }

}
