package com.ark.frigate.spring.envet;

import com.ark.frigate.spring.MessageDto;
import com.ark.frigate.spring.handler.MessageHandler;
import org.springframework.context.ApplicationEvent;

/**
 * @author zengweilong
 * @date 4/11/21 4:31 PM
 */
public class PublishMessageEvent extends ApplicationEvent {

    /**
     * 自定义处理类
     */
    private MessageHandler messageHandler;

    /**
     * 处理Bean
     */
    private MessageDto messageDto;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public PublishMessageEvent(Object source, MessageHandler handler, MessageDto messageDto) {
        super(source);
        this.messageHandler = handler;
        this.messageDto = messageDto;
    }


    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public MessageDto getMessageDto() {
        return messageDto;
    }

}
