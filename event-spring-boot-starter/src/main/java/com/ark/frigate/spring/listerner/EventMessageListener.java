package com.ark.frigate.spring.listerner;

import com.ark.frigate.spring.DingTalkMessageJson;
import com.ark.frigate.spring.envet.PublishMessageEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;


/**
 * 事件分发
 *
 * @author zengweilong
 */
@Slf4j
public class EventMessageListener {


    @EventListener
    public void publishEvent(PublishMessageEvent messageEvent) {

        try {

            log.info("事件监听,参数:{}", DingTalkMessageJson.toJson(messageEvent));

            if (null == messageEvent.getMessageDto()) {
                log.error("消息数据为空,不做处理");
                return;
            }

            // 优先处理通过接口实现的
            if (null == messageEvent.getMessageHandler()) {
                log.error("无对应的处理器,不做处理");
                return;
            }

            messageEvent.getMessageHandler().publishMessage(messageEvent.getMessageDto());

        } catch (Exception e) {
            log.error("事件监听异常:{}", messageEvent, e);
        }
    }
}
