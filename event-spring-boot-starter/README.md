# 引入Maven 坐标
```xml

	<dependency>
            <groupId>com.ark.frigate</groupId>
            <artifactId>event-spring-boot-starter</artifactId>
            <version>1.0-SNAPSHOT</version>
	</dependency>

```

#主要功能

向Spring监听发送事件

#使用方法

```java

@Service
public class PublishMessageProvider {

    @Autowired
    ApplicationContext applicationContext;

    public void youMethod(){
        SysMessageBO message = new SysMessageBO();
        
        /**
         *  AuditMessageHandler类必须实现MessageHandler接口， 
         * 
         **/
         
        AuditMessageHandler handler = applicationContext.getBean(AuditMessageHandler.class);
        applicationContext.publishEvent(new PublishMessageEvent(this, handler, message));
        
        /* 以上代码通过SpringBoot事件监听机制自动调用AuditMessageHandler类的publishMessage(SysMessageBO messageBO)方法 
        messageBO参数为 传入的 message  */
    }
    
}
    
```

```java

// AuditMessageHandler类定义

@Component
public class AuditMessageHandler implements MessageHandler {

    @Override
    public void publishMessage(SysMessageBO messageBO) {
         
         // TODO
             
    }

}
```