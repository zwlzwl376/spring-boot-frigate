# 引入Maven 坐标

```xml

	<dependency>
            <groupId>com.ark.tools</groupId>
            <artifactId>tools-spring-boot-starter</artifactId>
            <version>1.0-SNAPSHOT</version>
	</dependency>

```

# 配置 application.properties
```properties

swagger.enable=true                 // web api 文档开关
swagger.base-package=com.ark.place.controller           // web api 文档扫描路径

```

例如：application.yml

```yaml

swagger:
  enable: true
  base-package: com.ark.place.controller
 
```


#使用方法

```

  访问 ： http://127.0.0.1:{your Post}/doc.html
  
  用法参考 ：https://swagger.io/ 官方文档
  

```

#全局异常捕获

```java

import com.ark.frigate.tools.common.exception.GlobalAbstractExceptionHandle;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class MyExceptionHandler extends GlobalAbstractExceptionHandle {

    @Override
    void sendOnOffMessage(String loggerMessage, Exception ex) {

        // TODO 
        // 这里自定义将消息和 异常进行处理
        // 可以结合钉钉报警进行异常消息报警

    }
    

}
```
