package com.ark.frigate.tools.common.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * 删除参数
 *
 * @author zengweilong
 * @date 3/17/21 7:18 PM
 */
@Data
@ApiModel("请求删除实体,可批量")
public class DeleteVo {

    @ApiModelProperty(value = "删除时主键不能为空", required = true)
    @Size(max = 20, message = "一次最多删除20条数据")
    @NotEmpty(message = "删除ID不能为空")
    private Set<@Positive(message = "ID必须大于0") Long> idList;

}
