package com.ark.frigate.tools.utils;

/**
 * Object utils tools
 *
 * @author zengweilong
 */
public class NumericUtils {

    /**
     * lon == null :true
     *
     * @param lon
     * @return
     */
    public static boolean isNull(Object lon) {

        return lon == null;
    }

    /**
     * lon == null :false
     *
     * @param lon
     * @return
     */
    public static boolean isNotNull(Object lon) {

        return !isNull(lon);
    }

    /**
     * lon == null || lon == 0 :true
     *
     * @param lon
     * @return
     */
    public static boolean isNullOrZero(Long lon) {

        return (lon == null || lon == 0);
    }

    /**
     * lon == null || lon == 0 :false
     *
     * @param lon
     * @return
     */
    public static boolean isNotNullOrZero(Long lon) {

        return !isNullOrZero(lon);
    }

    /**
     * lon == null || lon == 0 :true
     *
     * @param lon
     * @return
     */
    public static boolean isNullOrZero(Integer lon) {

        return (lon == null || lon == 0);
    }

    /**
     * lon == null || lon == 0 :false
     *
     * @param lon
     * @return
     */
    public static boolean isNotNullOrZero(Integer lon) {

        return !isNullOrZero(lon);
    }

    /**
     * lon == null || lon == 0 :true
     *
     * @param lon
     * @return
     */
    public static boolean isNullOrZero(Double lon) {

        return (lon == null || lon == 0);
    }

    /**
     * lon == null || lon == 0 :false
     *
     * @param lon
     * @return
     */
    public static boolean isNotNullOrZero(Double lon) {

        return !isNullOrZero(lon);
    }

    /**
     * lon == null || lon == 0 :true
     *
     * @param lon
     * @return
     */
    public static boolean isNullOrZero(Float lon) {

        return (lon == null || lon == 0);
    }

    /**
     * lon == null || lon == 0 :false
     *
     * @param lon
     * @return
     */
    public static boolean isNotNullOrZero(Float lon) {

        return !isNullOrZero(lon);
    }

}
