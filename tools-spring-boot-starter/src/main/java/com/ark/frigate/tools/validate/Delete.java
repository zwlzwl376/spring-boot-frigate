package com.ark.frigate.tools.validate;

import javax.validation.groups.Default;

/**
 * 删除标记
 *
 * @author zengweilong
 * @date 3/17/21 6:22 PM
 */
public interface Delete extends Default {
}
