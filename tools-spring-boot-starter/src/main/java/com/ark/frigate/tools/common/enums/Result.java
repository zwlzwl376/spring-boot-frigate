package com.ark.frigate.tools.common.enums;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author zengweilong
 * @date 4/14/21 11:41 AM
 */
public enum Result {

    YES(100, "YES"),

    NO(200, "NO");

    private Integer code;

    private String text;

    Result(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public static Result getByCode(Integer code) {
        for (Result enums : Result.values()) {
            if (enums.code.equals(code)) {
                return enums;
            }
        }
        return null;
    }

    public static Map<Object, Object> getMap() {
        Map<Object, Object> options = Maps.newLinkedHashMap();
        for (Result result : Result.values()) {
            options.put(result.code, result.text);
        }
        return options;
    }
}
