package com.ark.frigate.tools.response;


import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @author zhanghuan
 * @date 2021-10-18
 */
@Data
public class ResponseData<T> {
    private Boolean success;
    private String code;
    private String message;
    private T data;

    public ResponseData() {
        super();
    }

    public ResponseData(T data) {
        this(true, HttpStatus.OK.value()+"", "request successfully", data);
    }

    public ResponseData(Boolean success, String code, String message) {
        this(success, code, message, null);
    }

    public ResponseData(Boolean success, String code, String message, T data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 构建response data
     *
     * @param data
     * @return
     */
    public static <T> ResponseData<T> buildSuccessResp(T data) {
        return buildResp(true, HttpStatus.OK.value()+"", "request successfully", data);
    }

    public static <T> ResponseData<T> buildResp(Boolean isSuccess, String code, String message, T data) {
        ResponseData<T> responseData = new ResponseData<>();
        responseData.setSuccess(isSuccess);
        responseData.setCode(code);
        responseData.setMessage(message);
        responseData.setData(data);
        return responseData;
    }

    public static ResponseData buildErrorResp(String code, String errorMessage) {
        return buildResp(false, code, errorMessage, null);
    }

    public static ResponseData buildErrorResp(String errorMessage) {
        return buildErrorResp(HttpStatus.BAD_REQUEST.value() + "", errorMessage);
    }
}
