package com.ark.frigate.tools.common.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 订单状态统计
 *
 * @author zengweilong
 * @date 2021/4/6 3:24 PM
 */
@Data
public class StatusCountDto {

    @ApiModelProperty("统计状态")
    private Integer status;

    @ApiModelProperty("统计结果")
    private Integer total;
}
