package com.ark.frigate.tools.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * TODO
 *
 * @author zengweilong
 * @date 9/14/21 10:50 AM
 */
@Slf4j
public class FileUtils {


    public static void writeFileMethod1(String filePath, String content, boolean append) {
        BufferedWriter wr = null;
        try {
            StopWatch stopWatch = StopWatchUtil.getStopWatch();
            wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath, append)));
            wr.write(content);
            wr.flush();
            wr.close();
            log.info("append to file={}, user-time={}", filePath, StopWatchUtil.getTotalTimeMillis(stopWatch));
        } catch (Exception e) {
            log.error("文件写入异常={}", filePath, e);
        } finally {
            if (wr != null) {
                try {
                    wr.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public static String readFileMethod1(String filePath) {
        BufferedReader br = null;
        try {
            StopWatch stopWatch = StopWatchUtil.getStopWatch();
            br = Files.newBufferedReader(Paths.get(filePath));
            StringBuffer readString = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                readString.append(line + "\n");
            }
            br.close();
            log.info("read to file={}, user-time={}", filePath, StopWatchUtil.getTotalTimeMillis(stopWatch));
            return readString.toString();
        } catch (Exception e) {
            log.error("文件读取异常={}", filePath, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return "";
    }


    public static void writeFileMethod2(String filePath, String content, boolean append) {
        FileWriter wr = null;
        try {
            StopWatch stopWatch = StopWatchUtil.getStopWatch();
            wr = new FileWriter(filePath, append);
            wr.write(content);
            wr.flush();
            wr.close();
            log.info("append to file={}, user-time={}", filePath, StopWatchUtil.getTotalTimeMillis(stopWatch));
        } catch (Exception e) {
            log.error("文件写入异常={}", filePath, e);
        } finally {
            if (wr != null) {
                try {
                    wr.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public static String readFileMethod2(String filePath) {
        FileReader fr = null;
        try {
            StopWatch stopWatch = StopWatchUtil.getStopWatch();
            fr = new FileReader(new File(filePath));
            StringBuffer readString = new StringBuffer();
            char[] buf = new char[1024];
            int len;
            while ((len = fr.read(buf)) != -1) {
                readString.append(new String(buf, 0, len));
            }
            fr.close();
            log.info("read to file={}, user-time={}", filePath, StopWatchUtil.getTotalTimeMillis(stopWatch));
            return readString.toString();
        } catch (Exception e) {
            log.error("文件读取异常={}", filePath, e);
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return "";
    }

    public static void writeFileMethod3(String filePath, String content, boolean append) {
        FileOutputStream fos = null;
        try {
            StopWatch stopWatch = StopWatchUtil.getStopWatch();
            fos = new FileOutputStream(filePath, append);
            fos.write(content.getBytes());
            fos.flush();
            fos.close();
            log.info("write to file={}, user-time={}", filePath, StopWatchUtil.getTotalTimeMillis(stopWatch));
        } catch (Exception e) {
            log.error("文件写入异常={}", filePath, e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }


    public static String readFileMethod3(String filePath) {
        FileInputStream fis = null;
        try {
            StopWatch stopWatch = StopWatchUtil.getStopWatch();
            fis = new FileInputStream(new File(filePath));
            StringBuffer readString = new StringBuffer();
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                readString.append(new String(bytes, 0, len));
            }
            fis.close();
            log.info("read to file={}, user-time={}", filePath, StopWatchUtil.getTotalTimeMillis(stopWatch));
            return readString.toString();
        } catch (Exception e) {
            log.error("文件读取异常={}", filePath, e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return "";
    }
}
