package com.ark.frigate.tools.validate;

import javax.validation.groups.Default;

/**
 * 编辑标记
 *
 * @author zengweilong
 * @date 3/17/21 6:00 PM
 */
public interface Editor extends Default {
}
