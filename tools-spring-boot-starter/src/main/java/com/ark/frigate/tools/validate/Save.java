package com.ark.frigate.tools.validate;

import javax.validation.groups.Default;

/**
 * 保存标记
 *
 * @author zengweilong
 * @date 3/17/21 10:28 PM
 */
public interface Save extends Default {
}
