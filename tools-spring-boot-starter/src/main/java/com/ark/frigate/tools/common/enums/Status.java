package com.ark.frigate.tools.common.enums;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author zengweilong
 * @date 4/14/21 11:41 AM
 */
public enum Status {

    NORMAL(100, "已开启"),

    DELETE(200, "已删除"),

    CLOSE(300, "已关闭");

    private Integer code;

    private String text;

    Status(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public static String getTextByCode(Integer code) {
        for (Status status : Status.values()) {
            if (status.code.equals(code)) {
                return status.text;
            }
        }
        return StringUtils.EMPTY;
    }

    public static Map<Object, Object> getMap() {
        Map<Object, Object> options = Maps.newLinkedHashMap();
        for (Status value : Status.values()) {
            options.put(value.code, value.text);
        }
        return options;
    }
}