package com.ark.frigate.tools.utils;

import org.springframework.util.StopWatch;

/**
 * TODO
 *
 * @author zengweilong
 * @date 9/14/21 10:50 AM
 */
public class StopWatchUtil {

    public static StopWatch getStopWatch() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        return stopWatch;
    }

    public static long getTotalTimeMillis(StopWatch stopWatch) {
        stopWatch.stop();
        return stopWatch.getTotalTimeMillis();
    }

    public static double getTotalTimeSeconds(StopWatch stopWatch) {
        stopWatch.stop();
        return stopWatch.getTotalTimeSeconds();
    }

    public static double getTotalTimeNanos(StopWatch stopWatch) {
        stopWatch.stop();
        return stopWatch.getTotalTimeNanos();
    }
}
