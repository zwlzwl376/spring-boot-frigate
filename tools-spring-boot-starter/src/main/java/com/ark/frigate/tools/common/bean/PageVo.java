package com.ark.frigate.tools.common.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.math.NumberUtils;

import javax.validation.ValidationException;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

/**
 * 分页通用实体
 *
 * @author zengweilong
 * @date 3/17/21 5:29 PM
 */
@Data
public class PageVo {

    @ApiModelProperty(value = "请求页 默认 1", notes = "请求页 默认 1", required = true)
    @NotNull(message = "请求页数不能为空，默认值1")
    @PositiveOrZero(message = "请求页必须是正整数.")
    private Integer pageNum = 1;

    @ApiModelProperty(value = "每页显示条数 默认 20", notes = "每页显示条数 默认 20", required = true)
    @NotNull(message = "每页显示条数不能为空，默认条数20")
    @PositiveOrZero(message = "每页显示条数必须是正整数. 最大500")
    @Max(value = 500, message = "最大500")
    private Integer pageSize = 20;

    public void setPageNum(Integer pageNum) {
        if (pageNum != null) {
            this.pageNum = pageNum;
        }
        if (this.pageNum <= NumberUtils.INTEGER_ZERO) {
            this.pageNum = NumberUtils.INTEGER_ONE;
        }
    }

    public void setPageSize(Integer pageSize) {
        if (pageSize != null) {
            this.pageSize = pageSize;
            return;
        }
        if (this.pageSize < NumberUtils.INTEGER_ZERO) {
            this.pageSize = NumberUtils.INTEGER_ZERO;
            return;
        }
        int maxPageSize = 500;
        if (this.pageSize > maxPageSize) {
            throw new ValidationException("PageSize is Too Big");
        }
    }
}
