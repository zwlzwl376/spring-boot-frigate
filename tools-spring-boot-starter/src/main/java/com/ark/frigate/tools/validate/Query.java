package com.ark.frigate.tools.validate;

import javax.validation.groups.Default;

/**
 * 查询标记
 *
 * @author zengweilong
 * @date 3/17/21 6:22 PM
 */
public interface Query extends Default {
}
