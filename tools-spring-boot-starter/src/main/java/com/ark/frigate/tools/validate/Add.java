package com.ark.frigate.tools.validate;

import javax.validation.groups.Default;

/**
 * 添加标记
 *
 * @author zengweilong
 * @date 3/17/21 5:59 PM
 */
public interface Add extends Default {
}
