package com.ark.frigate.tools;

/**
 * 字符定义
 *
 * @author zengweilong
 * @date 2021/3/28 4:21 PM
 */
public class Constant {

    public final static String URL_BAR = "/";

    public final static String COMMA = ",";

    public final static String SPOT = ".";

    public final static String TIME_PIX = ":";

    public final static String LINE = "_";
}
