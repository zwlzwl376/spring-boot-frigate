package com.ark.frigate.tools.context;

import lombok.Data;

@Data
public class RequestContext {
    /**
     * 登陆用户ID
     */
    private String userId;

    /**
     * 租户ID
     **/
    private String tenantId;

    /**
     * 组织结构 ID
     **/
    private String organizationId;
}
