package com.ark.frigate.tools.utils;

import org.springframework.web.method.HandlerMethod;

import java.lang.annotation.Annotation;

/**
 * 注解工具类
 *
 * @author Along
 */
public class AnnotationUtils {

    /**
     * 优先从方法上获取注解，方法上没有从类上面获取
     *
     * @param handlerMethod
     * @param annotationType
     * @param <A>
     * @return
     */
    public static <A extends Annotation> A getAnnotation(HandlerMethod handlerMethod, Class<A> annotationType) {

        A restInterface = handlerMethod.getMethodAnnotation(annotationType);
        if (restInterface == null) {
            restInterface = handlerMethod.getBeanType().getAnnotation(annotationType);
        }
        return restInterface;
    }

}
