package com.ark.frigate.tools.context;

/**
 * @author zhaowanli
 * @date 2018/8/20
 */
public class RequestContextHolder {
    /**
     * 存放当前线程的请求上下文信息
     */
    private static ThreadLocal<RequestContext> requestContextThreadLocal = new ThreadLocal<>();

    /**
     * 设置request context
     *
     * @param requestContext
     */
    public static void put(RequestContext requestContext) {
        requestContextThreadLocal.set(requestContext);
    }

    /**
     * 获取当前的RequestContext对象
     *
     * @return 当前的RequestContext对象
     */
    public static RequestContext getRequestContext() {
        RequestContext context = requestContextThreadLocal.get();
        if (context == null) {
            return new RequestContext();
        }
        return context;
    }

    /**
     * remove context
     */
    public static void remove() {
        requestContextThreadLocal.remove();
    }
}
