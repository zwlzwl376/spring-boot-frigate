import com.ark.frigate.tools.utils.HttpClientUtils;
import org.springframework.util.StopWatch;

/**
 * TODO
 *
 * @author zengweilong
 * @date 7/22/21 10:36 AM
 */
public class HttpTest {

    public static void main(String[] args) {
        String url = "http://127.0.0.1:9991/getSequenceCode/PB_CUSTOMER";
        StopWatch watch = new StopWatch();
        watch.start();
        for (int i = 0; i < 1000000; i++) {
            String response = HttpClientUtils.get(url);
            System.out.println(response);
        }
        watch.stop();
        System.out.println(watch.getTotalTimeSeconds());
    }
}
